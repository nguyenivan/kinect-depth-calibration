#include "mex.h"
#include "matrix.h"
#include <Windows.h>
#include "NuiApi.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    HANDLE  m_pDepthStreamHandle;
    HRESULT hr;
    INuiCoordinateMapper* coordinateMapper;
	INuiSensor* m_pNuiInstance;
    
	// Get pointer to Kinect handles
    unsigned __int64 *MXadress;
    if(nrhs==0) {
        mexErrMsgTxt("Give Pointer to Kinect as input");
    }
    
    MXadress = (unsigned __int64*)mxGetData(prhs[0]);
    m_pDepthStreamHandle=((HANDLE*)MXadress[2])[0];
    int depthwidth=(int)MXadress[7];
	int depthheight=(int)MXadress[8];

    // Get the mapper
    coordinateMapper = ((INuiCoordinateMapper**)MXadress[3])[0];

	// Get the sensor
	m_pNuiInstance=((INuiSensor**)MXadress[0])[0]; 


    // Initialize Output image
    int Jdimsc[2]={depthheight, depthwidth};
    plhs[0] = mxCreateNumericArray(2, Jdimsc, mxUINT16_CLASS, mxREAL);
	unsigned short *Iout;
    Iout = (unsigned short*)mxGetData(plhs[0]);
 

	// Wait for a Depth_Image_Frame to arrive
    //const NUI_IMAGE_FRAME * pImageFrame = NULL;
    //hr = m_pNuiInstance->NuiImageStreamGetNextFrame(m_pDepthStreamHandle, 200, &pImageFrame );

	NUI_IMAGE_FRAME pImageFrame = { 0 };
    hr = m_pNuiInstance->NuiImageStreamGetNextFrame(m_pDepthStreamHandle, 0, &pImageFrame );
    if( FAILED( hr ) )
	{ 
		printf("Failed to get Frame\r\n");
	} 
	else
	{
		// Convert the Depth_Image_Frame to an output image
        //INuiFrameTexture * pTexture = pImageFrame->pFrameTexture;
		BOOL nearMode = false;
		INuiFrameTexture * pTexture;
        m_pNuiInstance->NuiImageFrameGetDepthImagePixelFrameTexture( m_pDepthStreamHandle, &pImageFrame, &nearMode, &pTexture );

        NUI_LOCKED_RECT LockedRect;
        pTexture->LockRect( 0, &LockedRect, NULL, 0 );
        if( LockedRect.Pitch != 0 ) {
            USHORT* pBuffer =  (USHORT*) LockedRect.pBits;
            NUI_DEPTH_IMAGE_PIXEL* depthPoints = reinterpret_cast<NUI_DEPTH_IMAGE_PIXEL *>(LockedRect.pBits);
			LONG * m_colorCoordinates;
			m_colorCoordinates = new LONG[depthwidth*depthheight*2];

			//NUI_COLOR_IMAGE_POINT* colorPoints;
			//coordinateMapper->MapDepthFrameToColorFrame( NUI_IMAGE_RESOLUTION_640x480, depthwidth * depthheight, depthPoints, NUI_IMAGE_TYPE_COLOR, NUI_IMAGE_RESOLUTION_640x480, depthwidth * depthheight, colorPoints );
			
			m_pNuiInstance->NuiImageGetColorPixelCoordinateFrameFromDepthPixelFrameAtResolution(
				NUI_IMAGE_RESOLUTION_640x480,
				NUI_IMAGE_RESOLUTION_640x480,
				depthwidth*depthheight,
				pBuffer,
				depthwidth*depthheight*2,
				m_colorCoordinates
			);

			int j=0;
			for(int y=0; y < depthheight; y++) {
                for(int x=0; x < depthwidth; x++) {
					int depthIndex = x + y*depthwidth;
					LONG colorInDepthX = m_colorCoordinates[depthIndex * 2];
                    LONG colorInDepthY = m_colorCoordinates[depthIndex * 2 + 1];
					if ( colorInDepthX >= 0 && colorInDepthX < depthwidth && colorInDepthY >= 0 && colorInDepthY < depthheight )
                    {
                        // calculate index into color array
                        int colorIndex = colorInDepthX*depthheight+ colorInDepthY;
						USHORT mappedVal = depthPoints[j].depth;j++;
						Iout[colorIndex]= mappedVal;
                    }
                }

            }
        }
        else {
            printf("Depth Buffer Length Error\r\n");
        }
        m_pNuiInstance -> NuiImageStreamReleaseFrame( m_pDepthStreamHandle, &pImageFrame );
    }
    
}
