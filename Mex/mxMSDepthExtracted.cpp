#include "mex.h"
#include "matrix.h"
#include <Windows.h>
#include "NuiApi.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    HANDLE  m_pDepthStreamHandle;
    HRESULT hr;
    INuiSensor* m_pNuiInstance;
	INuiCoordinateMapper* coordinateMapper;
	// Get pointer to Kinect handles
    unsigned __int64 *MXadress;
    if(nrhs==0) {
        mexErrMsgTxt("Give Pointer to Kinect as input");
    }
    MXadress = (unsigned __int64*)mxGetData(prhs[0]);
    m_pDepthStreamHandle=((HANDLE*)MXadress[2])[0];
    int depthwidth=(int)MXadress[7];
	int depthheight=(int)MXadress[8];

	// Get the sensor
	m_pNuiInstance=((INuiSensor**)MXadress[0])[0]; 

	// Get the mapper
    coordinateMapper = ((INuiCoordinateMapper**)MXadress[3])[0];

    // Initialize Output image
    int Jdimsc[2]={depthheight, depthwidth};
    plhs[0] = mxCreateNumericArray(2, Jdimsc, mxUINT16_CLASS, mxREAL);
	unsigned short *Iout;
    Iout = (unsigned short*)mxGetData(plhs[0]);
 		
	// Wait for a Depth_Image_Frame to arrive
 	NUI_IMAGE_FRAME pImageFrame = { 0 };
    hr = m_pNuiInstance->NuiImageStreamGetNextFrame(m_pDepthStreamHandle, 0, &pImageFrame );
    if( FAILED( hr ) )
	{ 
		printf("Failed to get Frame\r\n");
	} 
	else
	{
		// Convert the Depth_Image_Frame to an output image
		BOOL nearMode = false;
		INuiFrameTexture * pTexture;
        m_pNuiInstance->NuiImageFrameGetDepthImagePixelFrameTexture( m_pDepthStreamHandle, &pImageFrame, &nearMode, &pTexture );
        
		NUI_LOCKED_RECT LockedRect;
        pTexture->LockRect( 0, &LockedRect, NULL, 0 );
        if( LockedRect.Pitch != 0 ) {
            USHORT* pBuffer =  (USHORT*) LockedRect.pBits;
	        NUI_DEPTH_IMAGE_PIXEL* depthPoints = reinterpret_cast<NUI_DEPTH_IMAGE_PIXEL *>(LockedRect.pBits);
			NUI_DEPTH_IMAGE_POINT pDepthPoint;
			NUI_COLOR_IMAGE_POINT pColorPoint;
			 
			int j=0;
            int index;
            for(int x=0; x<Jdimsc[0]; x++) {
                for(int y=0; y<Jdimsc[1]; y++) {
					pDepthPoint.x = y;
					pDepthPoint.y = x;
					pDepthPoint.depth = depthPoints[j].depth;
					coordinateMapper->MapDepthPointToColorPoint( NUI_IMAGE_RESOLUTION_640x480, &pDepthPoint, NUI_IMAGE_TYPE_COLOR, NUI_IMAGE_RESOLUTION_640x480, &pColorPoint );
                    if (pColorPoint.x >= 0 && pColorPoint.x < 639 && pColorPoint.y >= 0 && pColorPoint.y < 479)
					{
                        if (depthPoints[j].playerIndex < 255 && depthPoints[j].playerIndex > 0 && depthPoints[j].depth > 0)
                        // if (depthPoints[j].playerIndex < 255 && depthPoints[j].playerIndex > 0)
                        {
                            index=pColorPoint.y+pColorPoint.x*Jdimsc[0];
                            //Iout[index]=pBuffer[j]; j++;
                            //Iout[index]=depthPoints[j].depth;
                            Iout[index]=1;
                        }
					}
					 j++;
                }
            }
        }
        else {
            printf("Depth Buffer Length Error\r\n");
        }
        m_pNuiInstance->NuiImageStreamReleaseFrame( m_pDepthStreamHandle, &pImageFrame );
    }
}
