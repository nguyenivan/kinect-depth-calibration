% Added fake laser scanner to example

function Example()

addpath('Mex')
addpath('utils')

% Start the Kinect Process
% if(~exist('KinectHandles','var'))
%     KinectHandles = mxMSCreateContext([2 1]);
% end

global depthVideo;
global colorVideo;
global segmentVideo;
depthVideo = VideoWriter('depth.avi');
colorVideo = VideoWriter('color.avi');
segmentVideo = VideoWriter('segment.avi');

open(depthVideo);
open(colorVideo);
open(segmentVideo);

KinectHandles = mxMSCreateContext([2 2]);

c = onCleanup(@()CleanItUp(KinectHandles));

% Get Photo and Depth-frame
I = mxMSPhoto(KinectHandles);
DM = mxMSDepthExtracted(KinectHandles);
DM = double(DM); DM = DM-min(DM(:)); DM = DM./(max(DM(:)) + eps);
D =  mxMSDepthMapped(KinectHandles);
D = double(D); D = D-min(D(:)); D = D./(max(D(:)) + eps);


% Normalize Depth map to 0..1 range
% raw = mxMSDepth(KinectHandles);
% D = raw; D = double(D); D = D-min(D(:)); D = D./(max(D(:)) + eps);
% [r, theta, x, y] = scan2d(raw);

figure(1);
% subplot(1,2,1),
h1 = imshow(I);
figure(2);
% subplot(1,2,2), 
h2 = imshow(D,'Colormap',jet);
figure(3);
h3 = imshow(DM,'Colormap',jet);
SE = strel('disk',  5);
SEA = [SE];
% Display 900 Frames
for i = 1:90000
    I = mxMSPhoto(KinectHandles);   
    DM = mxMSDepthExtracted(KinectHandles);
    DM = double(DM); DM = DM-min(DM(:)); DM = DM./(max(DM(:)) + eps);
    DM = imerode(DM,SEA);
    DM = imdilate(DM,SEA);
    D = mxMSDepthMapped(KinectHandles);
    D = double(D); D = D-min(D(:)); D = D./(max(D(:)) + eps);
    set(h3, 'CDATA', flipdim(DM, 2));
    set(h1, 'CDATA', flipdim(I, 2)); % Update and flip image horizontally
    set(h2, 'CDATA', flipdim(D, 2)); % Update and flip image horizontally
    pause(0.01);
    drawnow;
    writeVideo(segmentVideo, DM);
    writeVideo(depthVideo, D);
    writeVideo(colorVideo, I);
end


end

function CleanItUp(KinectHandles)
    global depthVideo;
    global segmentVideo;
    global colorVideo;
    mxMSDeleteContext(KinectHandles);
    close(depthVideo);
    close(colorVideo);
    close(segmentVideo);
end


% Stop the Kinect Process
%mxMSDeleteContext(KinectHandles);
